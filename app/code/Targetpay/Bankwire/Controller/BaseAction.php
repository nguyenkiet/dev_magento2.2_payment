<?php
namespace Targetpay\Bankwire\Controller;

use Targetpay\Core\TargetPayCore;

/**
 * Targetpay Bankwire Report Controller
 *
 * @method POST
 */
class BaseAction extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resoureConnection;

    /**
     * @var \Magento\Backend\Model\Locale\Resolver
     */
    protected $localeResolver;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $transaction;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $order;

    /**
     * @var \Targetpay\Bankwire\Model\Bankwire
     */
    protected $bankwire;

    /**
     * @var \Magento\Sales\Api\TransactionRepositoryInterface
     */
    protected $transactionRepository;
    
    /**
     * @var \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface
     */
    protected $transactionBuilder;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Backend\Model\Locale\Resolver $localeResolver
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\DB\Transaction $transaction
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Sales\Model\Order $order
     * @param \Targetpay\Bankwire\Model\Bankwire $bankwire
     * @param \Magento\Sales\Api\TransactionRepositoryInterface $transactionRepository
     * @param \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Backend\Model\Locale\Resolver $localeResolver,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order $order,
        \Targetpay\Bankwire\Model\Bankwire $bankwire,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\TransactionRepositoryInterface $transactionRepository,
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder
        ) 
    {
            parent::__construct($context);
            $this->resoureConnection = $resourceConnection;
            $this->checkoutSession = $checkoutSession;
            $this->localeResolver = $localeResolver;
            $this->scopeConfig = $scopeConfig;
            $this->transaction = $transaction;
            $this->transportBuilder = $transportBuilder;
            $this->order = $order;
            $this->bankwire = $bankwire;
            $this->transactionRepository = $transactionRepository;
            $this->transactionBuilder = $transactionBuilder;
    }
    /***
     * Use to check order from target pay
     * @return boolean
     */
    public function checkTargetPayResult($txId, $orderId)
    {
        $language = ($this->localeResolver->getLocale() == 'nl_NL') ? 'nl' : 'en';
        $testMode = (bool) $this->scopeConfig->getValue('payment/bankwire/testmode');
        $targetPay = new TargetPayCore(
            $this->bankwire->getMethodType(),
            $this->scopeConfig->getValue('payment/bankwire/rtlo'),
            $this->bankwire->getAppId(),
            $language,
            $testMode
            );
        $checksum = md5($txId . $this->scopeConfig->getValue('payment/bankwire/rtlo') . $this->bankwire->getSalt());
        $targetPay->checkPayment($txId, ['checksum' => $checksum, 'once' => 0]);

        $paymentStatus = (bool) $targetPay->getPaidStatus();
        if ($testMode) {
            $paymentStatus = true; // Always OK if in testmode
            $this->getResponse()->setBody("Testmode... ");
        }
        if ($paymentStatus) {
            $db = $this->resoureConnection->getConnection();
            $tableName   = $db->getTableName('targetpay');
            $sql = "UPDATE ".$tableName."
                SET `paid` = now() WHERE `order_id` = '" . $orderId . "'
                AND method='" . $this->bankwire->getMethodType() . "'
                AND `targetpay_txid` = '" . $txId . "'";
            $db->query($sql);
            /* @var \Magento\Sales\Model\Order $currentOrder */
            $currentOrder = $this->order->loadByIncrementId($orderId);
            if ($currentOrder->getState() != \Magento\Sales\Model\Order::STATE_PROCESSING)
            {
                $payment_message = __('OrderId: %1 - Targetpay transactionId: %2 - Total price: %3', 
                    $orderId, 
                    $txId, 
                    $currentOrder->getBaseCurrency()->formatTxt($currentOrder->getGrandTotal())
                    );
                // Add transaction for refunable
                $payment = $currentOrder->getPayment();
                $payment->setLastTransId($txId);
                $payment->setTransactionId($txId);
                $orderTransactionId = $payment->getTransactionId();
                $transaction = $this->transactionBuilder->setPayment($payment)
                                ->setOrder($currentOrder)
                                ->setTransactionId($payment->getTransactionId())
                                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_ORDER);
                $payment->addTransactionCommentsToOrder($transaction, $payment_message);
                $payment->setParentTransactionId($transaction->getTransactionId());
                $payment->save();
                // Invoice
                $invoice = $currentOrder->prepareInvoice();
                $invoice->register()->capture();
                $this->transaction->addObject($invoice)->addObject($invoice->getOrder())->save();
                $invoice->setTransactionId($payment->getTransactionId());
                // Save order
                $currentOrder->setIsInProcess(true);
                $currentOrder->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
                $currentOrder->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
                $currentOrder->addStatusToHistory(\Magento\Sales\Model\Order::STATE_PROCESSING, $payment_message, true);
                $invoice->setSendEmail(true);
                $currentOrder->save();
                $this->getResponse()->setBody("Paid... ");
            } 
            else 
            {
                $this->getResponse()->setBody("Already completed, skipped... ");
            }
            return true;
        } else {
            $this->getResponse()->setBody("Payment Error: " . $targetPay->getErrorMessage());
        }
        return false;
    }
    /**
     * Empty action
     *
     * @return void|string
     */
    public function execute()
    {
        return;
    }
}
