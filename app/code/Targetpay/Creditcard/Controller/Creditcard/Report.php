<?php
namespace Targetpay\Creditcard\Controller\Creditcard;

use Targetpay\Core\TargetPayCore;

/**
 * Targetpay Creditcard Report Controller
 *
 * @method POST
 */
class Report extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resoureConnection;

    /**
     * @var \Magento\Backend\Model\Locale\Resolver
     */
    private $localeResolver;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    private $transaction;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Magento\Sales\Model\Order
     */
    private $order;

    /**
     * @var \Targetpay\Creditcard\Model\Creditcard
     */
    private $creditcard;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Backend\Model\Locale\Resolver $localeResolver
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\DB\Transaction $transaction
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Sales\Model\Order $order
     * @param \Targetpay\Creditcard\Model\Creditcard $creditcard
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Backend\Model\Locale\Resolver $localeResolver,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order $order,
        \Targetpay\Creditcard\Model\Creditcard $creditcard
    ) {
        parent::__construct($context);
        $this->resoureConnection = $resourceConnection;
        $this->localeResolver = $localeResolver;
        $this->scopeConfig = $scopeConfig;
        $this->transaction = $transaction;
        $this->transportBuilder = $transportBuilder;
        $this->order = $order;
        $this->creditcard = $creditcard;
    }

    /**
     * When a customer return to website from Targetpay Creditcard gateway after a payment is marked as successful.
     * This is an asynchronous call.
     *
     * @return void|string
     */
    public function execute()
    {
        $orderId = (int)$this->getRequest()->getParam('order_id');
        $txId = (string)$this->getRequest()->getParam('trxid', null);
        if (!isset($txId)) {
            $this->getResponse()->setBody("invalid callback, trxid missing");
            return;
        }

        $db = $this->resoureConnection->getConnection();
        $tableName   = $db->getTableName('targetpay');
        $sql = "SELECT `paid` FROM ".$tableName."
                WHERE `order_id` = " . $db->quote($orderId) . " 
                AND `targetpay_txid` = " . $db->quote($txId) . " 
                AND method=" . $db->quote($this->creditcard->getMethodType());
        
        $result = $db->fetchAll($sql);
        if (!count($result)) {
            $this->getResponse()->setBody('transaction not found');
            return;
        }

        $alreadyPaid = ((!empty($result[0]['paid'])) ? true : false);

        if ($alreadyPaid) {
            $this->getResponse()->setBody('callback already processed');
            return;
        }

        $language = ($this->localeResolver->getLocale() == 'nl_NL') ? 'nl' : 'en';
        $testMode = (bool) $this->scopeConfig->getValue('payment/creditcard/testmode');
        $targetPay = new TargetPayCore(
            $this->creditcard->getMethodType(),
            $this->scopeConfig->getValue('payment/creditcard/rtlo'),
            $this->creditcard->getAppId(),
            $language,
            $testMode
        );
        $targetPay->checkPayment($txId);

        $paymentStatus = (bool) $targetPay->getPaidStatus();
        if ($testMode) {
            $paymentStatus = true; // Always OK if in testmode
            $this->getResponse()->setBody("Testmode... ");
        }

        if ($paymentStatus) {
            $sql = "UPDATE ".$tableName." 
                SET `paid` = now() WHERE `order_id` = '" . $orderId . "'
                AND method='" . $this->creditcard->getMethodType() . "'
                AND `targetpay_txid` = '" . $txId . "'";
            $db->query($sql);

            $currentOrder = $this->order->loadByIncrementId($orderId);
            if ($currentOrder->getState() != \Magento\Sales\Model\Order::STATE_PROCESSING)
            {
                $payment_message = __('OrderId: %1 - Targetpay transactionId: %2 - Total price: %3', 
                    $orderId, 
                    $txId, 
                    $currentOrder->getBaseCurrency()->formatTxt($currentOrder->getGrandTotal())
                    );
                // Add transaction for refunable
                $payment = $currentOrder->getPayment();
                $payment->setLastTransId($txId);
                $payment->setTransactionId($txId);
                $orderTransactionId = $payment->getTransactionId();
                $transaction = $this->transactionBuilder->setPayment($payment)
                                ->setOrder($currentOrder)
                                ->setTransactionId($payment->getTransactionId())
                                ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_ORDER);
                $payment->addTransactionCommentsToOrder($transaction, $payment_message);
                $payment->setParentTransactionId($transaction->getTransactionId());
                $payment->save();
                // Invoice
                $invoice = $currentOrder->prepareInvoice();
                $invoice->register()->capture();
                $this->transaction->addObject($invoice)->addObject($invoice->getOrder())->save();
                $invoice->setTransactionId($payment->getTransactionId());
                // Save order
                $currentOrder->setIsInProcess(true);
                $currentOrder->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
                $currentOrder->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
                $currentOrder->addStatusToHistory(\Magento\Sales\Model\Order::STATE_PROCESSING, $payment_message, true);
                $invoice->setSendEmail(true);
                $currentOrder->save();
                $this->getResponse()->setBody("Paid... ");
            } 
            else 
            {
                $this->getResponse()->setBody("Already completed, skipped... ");
            }
        } else {
            /* Send failure payment email to customer */
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $transport = $this->transportBuilder
                ->setTemplateIdentifier(
                    $this->scopeConfig->getValue('payment/creditcard/email_template/failure'),
                    $storeScope
                )
                ->setTemplateOptions([
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $currentOrder->getStoreId(),
                ])
                ->setTemplateVars(['order' => $currentOrder])
                ->setFrom([
                    'name' => $this->scopeConfig->getValue('trans_email/ident_support/name', $storeScope),
                    'email' => $this->scopeConfig->getValue('trans_email/ident_support/email', $storeScope)
                ])
                ->addTo($currentOrder->getCustomerEmail())
                ->getTransport();

            $transport->sendMessage();
            $this->getResponse()->setBody("Not paid " . $targetPay->getErrorMessage() . "... ");
        }

        echo "(Magento, 15-06-2016)";
        return;
    }
}
