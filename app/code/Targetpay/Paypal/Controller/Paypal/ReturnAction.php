<?php
namespace Targetpay\Paypal\Controller\Paypal;

use Magento\Framework\Controller\ResultFactory;
use Targetpay\Paypal\Controller\BaseAction;

/**
 * Targetpay Paypal ReturnAction Controller
 *
 * @method GET
 */
class ReturnAction extends BaseAction
{
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Targetpay\Paypal\Model\Paypal $paypal
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Backend\Model\Locale\Resolver $localeResolver,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order $order,
        \Targetpay\Paypal\Model\Paypal $paypal,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        parent::__construct($context, $resourceConnection, $localeResolver, $scopeConfig, $transaction, $transportBuilder, $order, $paypal, $checkoutSession);
    }

    /**
     * When a customer return to website from Targetpay Paypal gateway.
     *
     * @return void|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /* @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $txId = $this->getRequest()->getParam('trxid', null);
        if (!isset($txId)) {
            $txId = $this->getRequest()->getParam('paypalid', null);
        }
        if (!isset($txId)) {
            $this->checkoutSession->restoreQuote();
            return $resultRedirect->setPath('checkout/cart');
        }
        
        $orderId = (int) $this->getRequest()->get('order_id');
        $db = $this->resoureConnection->getConnection();
        $tableName   = $this->resoureConnection->getTableName('targetpay');
        $sql = "SELECT `paid` FROM ".$tableName." 
                WHERE `order_id` = " . $db->quote($orderId) . "
                AND `targetpay_txid` = " . $db->quote($txId) . "
                AND method=" . $db->quote($this->paypal->getMethodType());
        $result = $db->fetchAll($sql);
        
        if (isset($result[0]['paid']) && $result[0]['paid']) {            
            $this->_redirect('checkout/onepage/success', ['_secure' => true]);
        } else {
            if(parent::checkTargetPayResult($txId, $orderId)){
                $this->_redirect('checkout/onepage/success', ['_secure' => true, 'paid' => "1"]);
            } else {
                $this->checkoutSession->restoreQuote();
                $this->_redirect('checkout/cart', ['_secure' => true]);
            }
        }
    }
}
