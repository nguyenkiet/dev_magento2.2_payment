<?php
namespace Targetpay\Afterpay\Model;

use Targetpay\Core\TargetPayCore;
use Targetpay\Core\TargetPayRefund;
use Targetpay\Afterpay\Controller\AfterpayValidationException;

class Afterpay extends \Magento\Payment\Model\Method\AbstractMethod
{

    const METHOD_CODE = 'afterpay';

    const METHOD_TYPE = 'AFP';

    const APP_ID = 'f8ca4794a1792886bb88060ca0685c1e';

    protected $maxAmount = 10000;

    protected $minAmount = 0.84;

    /**
     * Tax applying percent
     * @var array
     */
    protected $array_tax = [
        1 => 21,
        2 => 6,
        3 => 0,
        4 => 'none'
    ];

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::METHOD_CODE;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isGateway = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canAuthorize = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canCapturePartial = false;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canRefund = false;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canVoid = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canUseInternal = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canUseCheckout = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canUseForMultishipping = true;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_canSaveCc = false;

    /**
     * Payment method type
     *
     * @var string
     */
    private $tpMethod = self::METHOD_TYPE;

    /**
     * Payment app id
     *
     * @var string
     */
    private $appId = self::APP_ID;

    /**
     *
     * @var \Magento\Framework\Url
     */
    private $urlBuilder;

    /**
     *
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     *
     * @var \Magento\Sales\Model\Order
     */
    private $order;

    /**
     *
     * @var \Magento\Framework\Locale\Resolver
     */
    private $localeResolver;

    /**
     *
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resoureConnection;

    /**
     * *
     * Status of payment
     *
     * @var unknown
     */
    private $status;

    /**
     * Transaction Id
     *
     * @var unknown
     */
    private $trxid;

    /**
     * Reject when $status = "Rejected"
     *
     * @var unknown
     */
    private $reject_error;

    /**
     * Redirect Url when $status = "Incomplete"
     *
     * @var unknown
     */
    private $redirect_url;

    /**
     * Get the return URL
     *
     * @var unknown
     */
    private $return_url;

    /**
     * Current request parameter
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Url $urlBuilder
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order $orderFactory
     * @param \Magento\Framework\Locale\Resolver $localeResolver
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param array $data
     *            @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Url $urlBuilder,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Locale\Resolver $localeResolver,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Magento\Framework\App\RequestInterface $requestInterface = null,
        array $data = []
        )
    {
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $resource, $resourceCollection, $data);

        $this->urlBuilder = $urlBuilder;
        $this->checkoutSession = $checkoutSession;
        $this->order = $order;
        $this->localeResolver = $localeResolver;
        $this->resoureConnection = $resourceConnection; 
        $this->request = $requestInterface;       
    }
    /***
     * Get product tax by Targetpay
     * @param unknown $val
     * @return number
     */
    private function getTax($val)
    {
        if(empty($val)) return 4; // No tax
        else if($val >= 21) return 1;
        else if($val >= 6) return 2;
        else return 3;
    }
    /**
     * Format phonenumber by NL/BE
     * 
     * @param unknown $country
     * @param unknown $phone
     * @return unknown
     */
    private static function format_phone($country, $phone) {
        $function = 'format_phone_' . strtolower($country);
        if(method_exists('Targetpay\Afterpay\Model\Afterpay', $function)) {
            return self::$function($phone);
        }
        else {
            echo "unknown phone formatter for country: ". $function;
            exit;
        }
        return $phone;
    }
    
    /**
     * Format phone number
     * 
     * @param unknown $phone
     * @return string|mixed
     */
    private static function format_phone_nld($phone) {
        // note: making sure we have something
        if(!isset($phone{3})) { return ''; }
        // note: strip out everything but numbers
        $phone = preg_replace("/[^0-9]/", "", $phone);
        $length = strlen($phone);
        switch($length) {
            case 9:
                return "+31".$phone;
                break;
            case 10:
                return "+31".substr($phone, 1);
                break;
            case 11:
            case 12:
                return "+".$phone;
                break;
            default:
                return $phone;
                break;
        }
    }
    
    /**
     * Format phone number
     * 
     * @param unknown $phone
     * @return string|mixed
     */
    private static function format_phone_bel($phone) {
        // note: making sure we have something
        if(!isset($phone{3})) { return ''; }
        // note: strip out everything but numbers
        $phone = preg_replace("/[^0-9]/", "", $phone);
        $length = strlen($phone);
        switch($length) {
            case 9:
                return "+32".$phone;
                break;
            case 10:
                return "+32".substr($phone, 1);
                break;
            case 11:
            case 12:
                return "+".$phone;
                break;
            default:
                return $phone;
                break;
        }
    }
    /**
     * Breadown street address
     * @param unknown $street
     * @return NULL[]|string[]|unknown[]
     */
    private static function breakDownStreet($street)
    {
        $out = [];
        $addressResult = null;
        preg_match("/(?P<address>\D+) (?P<number>\d+) (?P<numberAdd>.*)/", $street, $addressResult);
        if(!$addressResult) {
            preg_match("/(?P<address>\D+) (?P<number>\d+)/", $street, $addressResult);
        }
        $out['street'] = array_key_exists('address', $addressResult) ? $addressResult['address'] : null;
        $out['houseNumber'] = array_key_exists('number', $addressResult) ? $addressResult['number'] : null;
        $out['houseNumberAdd'] = array_key_exists('numberAdd', $addressResult) ? trim(strtoupper($addressResult['numberAdd'])) : null;
        return $out;
    }
    /**
     * Start payment
     *
     * @param integer $bankId
     *
     * @return string Bank url
     * @throws \Magento\Checkout\Exception
     */
    public function setupPayment($bankId = false)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->getOrder();

        if (! $order->getId()) {
            throw new \Magento\Checkout\Exception(__('Cannot load order #' . $order->getRealOrderId()));
        }

        if ($order->getGrandTotal() < $this->minAmount) {
            throw new \Magento\Checkout\Exception(__('Het totaalbedrag is lager dan het minimum van ' . $this->minAmount . ' euro voor ' . Afterpay::METHOD_CODE));
        }

        if ($order->getGrandTotal() > $this->maxAmount) {
            throw new \Magento\Checkout\Exception(__('Het totaalbedrag is hoger dan het maximum van ' . $this->maxAmount . ' euro voor ' . Afterpay::METHOD_CODE));
        }

        $orderId = $order->getRealOrderId();
        $language = ($this->localeResolver->getLocale() == 'nl_NL') ? "nl" : "en";
        $testMode = (bool) $this->_scopeConfig->getValue('payment/afterpay/testmode');

        $targetPay = new TargetPayCore($this->tpMethod, $this->_scopeConfig->getValue('payment/afterpay/rtlo'), $this->appId, $language, $testMode);
        $targetPay->setAmount(round($order->getGrandTotal() * 100));
        $targetPay->setDescription("Order #$orderId");
        $targetPay->setReturnUrl($this->urlBuilder->getUrl('afterpay/afterpay/return', [
            '_secure' => true,
            'order_id' => $orderId
        ]));
        $targetPay->setReportUrl($this->urlBuilder->getUrl('afterpay/afterpay/report', [
            '_secure' => true,
            'order_id' => $orderId
        ]));

        $this->return_url = $targetPay->getReturnUrl();
        $targetPay->bindParam('email', $order->getCustomerEmail());
        $targetPay->bindParam('userip', $_SERVER["REMOTE_ADDR"]);

        
        // Build invoice lines
        $invoice_lines = null;
        $total_amount_by_product = 0;
        foreach ($order->getAllItems() as $item) {
            $invoice_lines[] = [
                'productCode' => $item->getProduct()->getId(),
                'productDescription' => $item->getProduct()->getName(),
                'quantity' => (int) $item->getQtyOrdered(),
                'price' => $item->getPrice(),
                'taxCategory' => ($item->getPrice() > 0) ? $this->getTax(100 * $item->getTaxAmount() / $item->getPrice()) : 3
            ];
            $total_amount_by_product += $item->getPrice();
        }
        // Update to fix the total amount and item price
        if($total_amount_by_product < $order->getGrandTotal()){
            $invoice_lines[] = [
                'productCode' => "000000",
                'productDescription' => "Other fee (shipping, additional fees)",
                'quantity' => 1,
                'price' => $order->getGrandTotal() - $total_amount_by_product,
                'taxCategory' => 3
            ];
        }
        // Add invoice line to payment
        if($invoice_lines != null && !empty($invoice_lines)){
            $targetPay->bindParam('invoicelines', json_encode($invoice_lines));
        }
        // Fix country code to ISO 639-2
        $billingCountry = (strtoupper($order->getBillingAddress()->getCountryId()) == 'BE' ? 'BEL' : 'NLD');
        $shippingCountry = (strtoupper($order->getShippingAddress()->getCountryId()) == 'BE' ? 'BEL' : 'NLD');
        // Build billing address
        $billing_addresses = $order->getBillingAddress()->getStreet();
        $billing_address_1 = count($billing_addresses) > 0 ? $billing_addresses[0] : "";
        $billing_address_2 = count($billing_addresses) > 1 ? $billing_addresses[1] : "";
        $streetParts = self::breakDownStreet("");
        if(empty($billing_address_2)) {
            $streetParts = self::breakDownStreet($billing_address_1);
        }
        
        $targetPay->bindParam('billingstreet', empty($streetParts['street']) ? $billing_address_1 : $streetParts['street']);
        $targetPay->bindParam('billinghousenumber', empty($streetParts['houseNumber'].$streetParts['houseNumberAdd']) ? $billing_address_2 : $streetParts['houseNumber'].$streetParts['houseNumberAdd']);
        $targetPay->bindParam('billingpostalcode', $order->getBillingAddress()->getPostcode());
        $targetPay->bindParam('billingcity', $order->getBillingAddress()->getCity());
        $targetPay->bindParam('billingpersonemail', $order->getBillingAddress()->getEmail());
        $targetPay->bindParam('billingpersoninitials', "");
        $targetPay->bindParam('billingpersongender', "");
        $targetPay->bindParam('billingpersonsurname', $order->getBillingAddress()->getFirstname());
        $targetPay->bindParam('billingcountrycode', $billingCountry);
        $targetPay->bindParam('billingpersonlanguagecode', $billingCountry);
        $targetPay->bindParam('billingpersonbirthdate', "");
        $targetPay->bindParam('billingpersonphonenumber', self::format_phone($billingCountry, $order->getBillingAddress()->getTelephone()));
        // Build shipping address
        $shipping_addresses = $order->getShippingAddress()->getStreet();
        $shipping_address_1 = count($shipping_addresses) > 0 ? $shipping_addresses[0] : "";
        $shipping_address_2 = count($shipping_addresses) > 1 ? $shipping_addresses[1] : "";
        $streetParts = self::breakDownStreet("");
        if(empty($shipping_address_2)){
            $streetParts = self::breakDownStreet($shipping_address_1);
        }
        $targetPay->bindParam('shippingstreet', empty($streetParts['street']) ? $shipping_address_1 : $streetParts['street']);
        $targetPay->bindParam('shippinghousenumber', empty($streetParts['houseNumber'].$streetParts['houseNumberAdd']) ? $shipping_address_2 : $streetParts['houseNumber'].$streetParts['houseNumberAdd']);
        $targetPay->bindParam('shippinghousenumber', "");
        $targetPay->bindParam('shippingpostalcode', $order->getShippingAddress()->getPostcode());
        $targetPay->bindParam('shippingcity', $order->getShippingAddress()->getCity());
        $targetPay->bindParam('shippingpersonemail', $order->getShippingAddress()->getEmail());
        $targetPay->bindParam('shippingpersoninitials', "");
        $targetPay->bindParam('shippingpersongender', "");
        $targetPay->bindParam('shippingpersonsurname', $order->getShippingAddress()->getFirstname());
        $targetPay->bindParam('shippingcountrycode', $shippingCountry);
        $targetPay->bindParam('shippingpersonlanguagecode', $shippingCountry);
        $targetPay->bindParam('shippingpersonbirthdate', "");
        $targetPay->bindParam('shippingpersonphonenumber', self::format_phone($shippingCountry, $order->getShippingAddress()->getTelephone()));

        // Start payment
        $result = $targetPay->startPayment();

        if (! $result) {
            $exception = new AfterpayValidationException($targetPay->getErrorMessage());
            if ($exception->IsValidationError()) {
                throw $exception;
            } else {
                throw new \Exception(__("TargetPay error: {$targetPay->getErrorMessage()}"));
            }
        }

        $result_str = $targetPay->getMoreInformation();
        // Process return message
        list ($this->trxid, $this->status) = explode("|", $result_str);
        if (strtolower($this->status) != "captured") {
            list ($this->trxid, $this->status, $ext_info) = explode("|", $result_str);
            if (strtolower($this->status) == "rejected") {
                $this->reject_error = $ext_info;
            } else {
                $this->redirect_url = $ext_info;
            }
        }

        $db = $this->resoureConnection->getConnection();
        $tableName = $this->resoureConnection->getTableName('targetpay');
        $db->query("
            INSERT INTO " . $tableName . " SET
                `order_id`=" . $db->quote($orderId) . ",
                `method`=" . $db->quote($this->tpMethod) . ",
                `targetpay_txid`=" . $db->quote($this->trxid) . ",
                `targetpay_response` = " . $db->quote($this->status) . ",
                `more`=" . $db->quote($result_str));
        return $this;
    }

    /**
     * Get the status result
     *
     * @return \Targetpay\Afterpay\Model\unknown
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the transaction id
     *
     * @return \Targetpay\Afterpay\Model\unknown
     */
    public function getTransactionId()
    {
        return $this->trxid;
    }

    /**
     * Get the Rejected message when status is "Rejected"
     *
     * @return \Targetpay\Afterpay\Model\unknown
     */
    public function getRejectedMessage()
    {
        return $this->reject_error;
    }

    /**
     * Get the Url when status is "Incomplete"
     *
     * @return \Targetpay\Afterpay\Model\unknown
     */
    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    /**
     * Get the return url after starting payment
     *
     * @return \Targetpay\Afterpay\Model\unknown
     */
    public function getReturnUrl()
    {
        return $this->return_url;
    }

    /**
     * Retrieve payment method type
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getMethodType()
    {
        if (empty($this->tpMethod)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('We cannot retrieve the payment method type'));
        }
        return $this->tpMethod;
    }

    /**
     * Retrieve payment app id
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAppId()
    {
        if (empty($this->appId)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('We cannot retrieve the payment app id'));
        }
        return $this->appId;
    }

    /**
     * Retrieve current order
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        $orderId = $this->checkoutSession->getLastOrderId();
        return $this->order->load($orderId);
    }
    /**
     * Check refund availability
     *
     * @return bool
     * @api
     */
    public function canRefund()
    {
        return !empty ($this->_scopeConfig->getValue('payment/' .  self::METHOD_CODE . '/apitoken'));
    }
    /**
     * Check partial refund availability for invoice
     *
     * @return bool
     * @api
     */
    public function canRefundPartialPerInvoice()
    {
        return !empty($this->_scopeConfig->getValue('payment/' .  self::METHOD_CODE . '/apitoken'));
    }
    /**
     * Refund specified amount for payment
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $api_token = $this->_scopeConfig->getValue('payment/' .  self::METHOD_CODE . '/apitoken');
        $refundObj = new TargetPayRefund(self::METHOD_TYPE, $amount, $api_token, $payment, $this->resoureConnection);
        $refundObj->setAppId($this->appId);
        $refundObj->setLanguage(($this->localeResolver->getLocale() == 'nl_NL') ? "nl" : "en");
        $refundObj->setLayoutCode($this->_scopeConfig->getValue('payment/' .  self::METHOD_CODE . '/rtlo'));
        $refundObj->setTestMode((bool) $this->_scopeConfig->getValue('payment/' .  self::METHOD_CODE . '/testmode'));
        $refundObj->refund();
    }
}
